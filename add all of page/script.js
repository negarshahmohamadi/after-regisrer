const afterRegister = document.querySelector(".after-register");
const afterRegisterIcon = document.querySelector(".after-register i");
const registerMenu = document.querySelector(".register-menu");
const registerItem = document.querySelector(".register-item");

afterRegister.addEventListener("click", showRegisterMenu);
afterRegister.addEventListener("click", changeRigisterIcon);

function showRegisterMenu() {
    if (registerMenu.style.display === "block") {
        registerMenu.style.display = "none";
    } else {
        registerMenu.style.display = "block";
    }
}

function changeRigisterIcon() {
    if (afterRegisterIcon.classList.contains("fa-user")) {
        afterRegisterIcon.classList = "fa fa-times";
    } else {
        afterRegisterIcon.classList = "fa fa-user";
    }
}